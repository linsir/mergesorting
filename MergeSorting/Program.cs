﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MergeSorting
{

    public interface IValue
    {
        int Value { get; set; }
    }

    public class Item : IComparable<Item>, IValue
    {
        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }

        public int Value { get; set; }
    }

    public static class ListExtension
    {
        public static void Swap<T>(this List<T> list, int pre, int next)
        {
            T temp = list[pre];
            list[pre] = list[next];
            list[next] = temp;
        }

        public static string ListDebugStr<T>(this List<T> list) where T : IValue
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in list)
            {
                stringBuilder.Append(item.Value).Append(":");
            }

            return stringBuilder.ToString();
        }
    }

    public class AbstractItemList<T> where T : IComparable<T>, IValue
    {
        private List<T> _list = new List<T>();
        private List<T> _sortedList = new List<T>();

        public AbstractItemList(IEnumerable<T> items)
        {
            AddRange(items);
        }

        private void AddRange(IEnumerable<T> items)
        {
            _list.AddRange(items);
        }

        public void MergeSort()
        {
            List<T> list = MergeSort(_list, 0, _list.Count - 1);
            _sortedList.AddRange(list);
        }

        // 总结：递归的时候一定要控制住传入参数的状态，所有的操作都在传入参数上完成
        private List<T> MergeSort(List<T> list, int startIndex, int endIndex)
        {
            int listCount = list.Count;
            if (listCount < 2)
            {
                return list;
            }

            if (listCount == 2)
            {
                if (list[endIndex].CompareTo(list[startIndex]) < 0)
                    list.Swap(startIndex, endIndex);

                return list;
            }

            int m = listCount / 2;
            List<T> leftList = list.GetRange(0, m);
            List<T> rightList = list.GetRange(m, endIndex - m + 1);
            Console.WriteLine($"leftList:{leftList.ListDebugStr()}");
            Console.WriteLine($"rightList:{rightList.ListDebugStr()}");
            var mergedLeftList = MergeSort(leftList, 0, leftList.Count - 1);
            var mergedRightList = MergeSort(rightList, 0, rightList.Count - 1);
            return Merge(mergedLeftList, mergedRightList);
        }

        private List<T> Merge(List<T> leftList, List<T> rightList)
        {
            int length = leftList.Count + rightList.Count;
            int left = 0;
            int right = 0;

            List<T> newList = new List<T>();

            for (int index = 0; index < length; ++index)
            {
                if (left < leftList.Count && leftList[left].CompareTo(rightList[right]) <= 0)
                {
                    newList.Insert(index, leftList[left]);
//                    newList.Add(leftList[left]);
                    left++;
                }
                else
                {
                    newList.Insert(index, rightList[right]);
//                    newList.Add(rightList[right]);
                    right++;
                    if (right == rightList.Count)
                        break;
                }
            }


            while (left < leftList.Count)
            {
                newList.Add(leftList[left]);
                left++;
            }

            while (right < rightList.Count)
            {
                newList.Add(rightList[right]);
                right++;
            }

            return newList;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            _sortedList.ForEach(item => { stringBuilder.Append(item.Value).Append("-"); });
            return stringBuilder.ToString();
        }
    }

    public class ItemList : AbstractItemList<Item>
    {
        public ItemList(IEnumerable<Item> items) : base(items)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Item[] items = new Item[]
            {
                new Item() {Value = 76},
                new Item() {Value = 45},
                new Item() {Value = 56},
                new Item() {Value = 45},
                new Item() {Value = 87},
                new Item() {Value = 87},
            };

            ItemList itemList = new ItemList(items);
            itemList.MergeSort();
            Console.WriteLine(itemList);
        }
    }
}